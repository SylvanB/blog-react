import React from "react";
import { Link } from "react-router-dom" 

export class About extends React.Component {
    render() {
        return (
            <div>
                <h1>Who Am I?</h1>
                <p>
                    My name is Sylvan Bowdler, I am an application developer at PebblePad who mainly meddles in the backend using 
                    C#.Aside from web development, I have interests in language design and software security and this site will document
                    my journey through this weird intersection I inhabit. 
                </p>
                <h2>Technologies I've used or am familiar with:</h2>
                <ul>
                    <li>C#</li>
                    <li>.Net</li>
                    <li>MongoDB</li>
                    <li>Rust</li>
                </ul>
                <h2>Where to find me?</h2>
                <p>
                    You can find me on the following social media sites:
                </p>
                <ul>
                    <li><Link to="https://twitter.com/goldensoulone1">Twitter</Link></li>
                    <li><Link to="https://twitter.com/goldensoulone1">Medium</Link></li>
                    <li><Link to="https://twitter.com/goldensoulone1">LinkedIn</Link></li>
                </ul>
            </div>
        );
    }
}
