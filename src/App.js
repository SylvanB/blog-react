import React from 'react';
import { NavBar } from "./NavBar";
import { Title } from "./Title";
import { PostContainer } from "./PostContainer";
import { Post } from "./Post";
import { About } from "./About";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import './App.css';

class App extends React.Component {
    render() {
        return (
            <Router>
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <link rel="manifest" href="/site.webmanifest" />

                <div className="App">
                    <header>
                        <Title title="My corner of the internetz."></Title>
                        <NavBar></NavBar>
                    </header>
                    <Switch>
                        <Route path="/about">
                            <About></About>
                        </Route>
                        <Route path="/posts">
                            <PostContainer></PostContainer>
                        </Route>
                        <Route path="/post/:id" component={Post}/>
                        {/* <Route path="*">
                            <NoMatch />
                        </Route> */}
                    </Switch>
                    <footer>
                        <p>...</p>
                    </footer>
                </div >
            </Router>
        );
    }

}

export default App;
