import React from "react";
import { PostItem } from "./PostItem";
import { BlogService } from "./Service/BlogService";

export class PostContainer extends React.Component {
    constructor() {
        super()
        this.blogService = new BlogService();
        this.state = {
            data: [],
        };
    }

    componentDidMount = async () => {
        this.setState({ data: await this.blogService.GetAllPostDigests() });
    }

    render = () => {
        if (this.state.data == null || this.state.data.length <= 0) {
            return (
                <div>
                    <h1>Posts</h1>
                    <p>None :(</p>
                </div>
            );
        }

        return (
            <div>
                <h1>Posts</h1>
                {this.state.data.map((digest, i) => {
                    let date = new Date(digest.date);
                    date = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
                    return (
                        <PostItem key={i} id={digest.id} title={digest.title} date={date} description={digest.description} />
                    );
                })}
            </div>
        );
    }
}
