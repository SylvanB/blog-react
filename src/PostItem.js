import React from "react";
import {
    Link,
} from "react-router-dom";

export class PostItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = { 
            date: this.props.date || new Date().toLocaleDateString(),
            title: this.props.title || "Untitled",
            description: this.props.description || "No description.",
        }
    }

    render() {
        return (
            <div>
                <Link to={"/post/" + this.props.id} ><h3>{this.state.date} - {this.state.title}</h3></Link>
                <p>{this.state.description}</p>
            </div>
        );
    }
}
