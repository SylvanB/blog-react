import React from "react";
import { BlogService } from "./Service/BlogService";
import ReactMarkdown from "react-markdown";

export class Post extends React.Component {
    constructor(props) {
        super(props)
        this.blogService = new BlogService();
        let id = this.props.match.params.id;
        console.log(id);
        this.state = {
            id: id,
            // date: this.props.date || new Date().toLocaleDateString(),
            // title: this.props.title || "Untitled",
            // description: this.props.description || "No description.",
        }
    }

    componentDidMount = async () => {
        let data = await this.blogService.GetPost(this.state.id);
        console.log(`Data:`);
        console.log(data);
        this.setState({
            date: new Date(data.date).toLocaleDateString() || new Date().toLocaleDateString(),
            title: data.title || "Untitled",
            description: data.description || "No description.",
            author: data.author || "Unnamed",
            content: data.content || "No content.",
        });
    }

    render = () => {
        return (
            <div>
                <h1>{this.state.title}</h1>
                <p>Posted on {this.state.date} by {this.state.author}</p>
                {/* <p>{this.state.content}</p> */}
                {/* <ReactMarkdown
                    // className="result"
                    source={this.state.content}
                    // skipHtml={this.state.htmlMode === 'skip'}
                    // escapeHtml={this.state.htmlMode === 'escape'}
                /> */}
                <ReactMarkdown
                    source={this.state.content}
                />
            </div>
        );
    }
}
// const PostWithRouter = withRouter(Post);
// export { PostWithRouter as Post };
