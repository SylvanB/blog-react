export class BlogService {

    GetAllPostDigests = async () => {
        let resp =  await fetch("https://sylvanb.dev/api/Posts/All");
        return await resp.json();
    }

    GetPost = async (id) => {
        let resp =  await fetch("https://sylvanb.dev/api/Posts/" + id);
        return await resp.json();
    }

}