import React from "react";
import PropTypes from 'prop-types';
import {
    Link,
} from "react-router-dom";

export class Title extends React.Component {
    
    render() {
        return (
            <h1 className="title"><Link to="/posts">{this.props.title}</Link></h1>
        );
    }
}

Title.propTypes = {
    title: PropTypes.string,
}
