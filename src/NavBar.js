import React from "react";
import {
    Link,
} from "react-router-dom";


export class NavBar extends React.Component {
    render() {
        return (
            <ul className="nav-container">
                <li className="nav-item"><Link to="/posts">Posts</Link></li>
                <li className="nav-item"><Link to="/about">About Me</Link></li>
            </ul>
        );
    }
}

